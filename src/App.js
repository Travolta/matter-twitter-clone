import React, { Component } from 'react'
import { ComposeNote } from './components/ComposeNote'
import { DeleteButton } from './components/ClearStorage'
import { NoteList } from './components/NoteList'

import './App.css'

class App extends Component {
    constructor() {
        super()

        this.state = {
            nextId: 1,
            nextCommentId: 1,
            notes: {},
            comments: {}
        }

        this.clearState = this.clearState.bind(this)
        this.updateNotes = this.updateNotes.bind(this)
        this.updateComments = this.updateComments.bind(this)
        this.hyrdateStateWithLocalStorage = this.hyrdateStateWithLocalStorage.bind(this)
        this.likeNotes = this.likeNotes.bind(this)
    }

    hyrdateStateWithLocalStorage() {
        for (let key in this.state) {
            if (localStorage.hasOwnProperty(key)) {
                let value = localStorage.getItem(key)

                try {
                    value = JSON.parse(value)
                    this.setState({ [key]: value })
                } catch (error) {
                    this.setState({ [key]: value })
                }
            }
        }
    }
    updateNotes(note) {
        const newState = {
            notes: {
                ...this.state.notes,
                [this.state.nextId]: note
            }
        }
        this.setState(newState)

        const nextId = this.state.nextId + 1
        this.setState({ nextId })

        localStorage.setItem('notes', JSON.stringify(newState.notes))
        localStorage.setItem('nextId', nextId)
    }

    updateComments(comment) {
        const newState = {
            comments: {
                ...this.state.comments,
                [this.state.nextCommentId]: comment
            }
        }
        this.setState(newState)

        const nextCommentId = this.state.nextCommentId + 1
        this.setState({ nextCommentId })

        localStorage.setItem('comments', JSON.stringify(newState.comments))
        localStorage.setItem('nextCommentId', nextCommentId)
    }

    likeNotes(id) {
        const liked = !this.state.notes[id].liked
        const newState = {
            notes: {
                ...this.state.notes,
                [id]: {
                    ...this.state.notes[id],
                    liked
                }
            }
        }

        this.setState(newState)

        localStorage.setItem('liked', JSON.stringify(newState))
    }

    clearState() {
        this.setState({ nextId: 1, nextCommentId: 1, notes: {}, comments: {} })
    }

    componentWillMount() {
        this.hyrdateStateWithLocalStorage()
    }


    render() {
        return (
            <div className="app">
                <header>
                    <h1
                        className="header">
                        Share things that <em>Matter</em>
                    </h1>
                </header>
                <div>
                    <p
                        className="matters-header">
                        What <em>Matters</em> to you?
                    </p>
                    <ComposeNote
                        updateNotes={this.updateNotes}
                        id={this.state.nextId}
                    />
                    <NoteList
                        notes={this.state.notes}
                        comments={this.state.comments}
                        updateComments={this.updateComments}
                        likeNote={this.likeNotes}
                    />
                </div>
                <div>
                    <DeleteButton
                        clearState={this.clearState}
                    />
                </div>
            </div>
        )
    }
}

export default App
