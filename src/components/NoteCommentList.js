import React from 'react'
import moment from 'moment'

import '../App.css'

export class NoteCommentList extends React.Component {
    constructor(props) {
        super(props)

        this.displayNoteCommentlist = this.displayNoteCommentlist.bind(this)
    }

    displayNoteCommentlist(comment) {
        const orderedCommentsDisplay = []
        for (const value of comment) {
            orderedCommentsDisplay.push(
                <li
                    className="comment-list-item"
                    key={value.postDate}
                >
                    <span
                        className="comment-text">
                        {value.text}
                    </span>
                    <br />
                    <span
                        className="comment-date">
                        {moment(value.postDate).fromNow()}
                    </span>
                </li>
            )
        }

        return orderedCommentsDisplay
    }

    render() {
        const comments = this.displayNoteCommentlist(this.props.orderedComments)

        return (
            <>
                <p
                    className="comments-heading">
                    Comments:
            </p>
                <ul
                    className="comments-list">
                    {comments}
                </ul>
            </>
        )
    }
}

