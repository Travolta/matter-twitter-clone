import React from 'react';

import '../App.css'

export class DeleteButton extends React.Component {
    constructor(props) {
        super(props);

        this.deleteAll = this.deleteAll.bind(this)
    }

    deleteAll() {
        localStorage.clear()
        this.props.clearState()
    }

    render() {
        return <button
            className="deleteall-button"
            onClick={this.deleteAll}>
            Delete all notes
        </button>
    }
}