import React from 'react'
import moment from 'moment'

import { LikeButton } from './LikeButton'
import { ComposeComment } from './ComposeComment'
import { NoteCommentList } from './NoteCommentList'

import '../App.css'

export class NoteList extends React.Component {
    constructor(props) {
        super(props)

        this.displayNotelist = this.displayNotelist.bind(this)
    }

    getCurrentNotesComments(noteId) {
        const commentIdsArray = Object.keys(this.props.comments).sort((a, b) => b - a)
        const orderedComments = []

        for (const id of commentIdsArray) {
            if (this.props.comments[id].noteId === noteId) {
                orderedComments.push(
                    this.props.comments[id]
                )
            }
        }

        return orderedComments
    }

    displayNotelist(note) {
        const noteIdsArray = Object.keys(note).sort((a, b) => b - a)
        const orderedNotes = []

        for (const id of noteIdsArray) {
            const orderedComments = this.getCurrentNotesComments(id)
            orderedNotes.push(
                <li
                    className="note-list-item"
                    key={id}
                >
                    <span
                        className="note-text">
                        {note[id].text}
                    </span>
                    <br />
                    <span
                        className="note-postdate">
                        {moment(note[id].date).fromNow()}
                    </span>
                    <br />
                    <div className="like-button-And-comment-input">
                        <LikeButton
                            note={this.props.notes}
                            noteid={id}
                            likeNote={this.props.likeNote}
                        />
                        <ComposeComment
                            noteid={id}
                            updateComments={this.props.updateComments}
                        />
                    </div>
                    {orderedComments.length > 0 &&
                        <NoteCommentList
                            orderedComments={orderedComments}
                        />}
                </li>
            )
        }

        return orderedNotes
    }

    render() {
        const notes = this.props.notes ? this.displayNotelist(this.props.notes) : []

        return <ul
            className="note-list">
            {notes}
        </ul>
    }
}

