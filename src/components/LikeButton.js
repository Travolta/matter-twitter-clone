import React from 'react'

import '../App.css'

export class LikeButton extends React.Component {
    constructor(props) {
        super(props)

        this.handleClick = this.handleClick.bind(this)
    }

    handleClick() {
        this.props.likeNote(this.props.noteid)
    }

    render() {
        const likedSymbol = this.props.note[this.props.noteid].liked ? "<3" : "</3"
        return (
            <button
                className="like-button"
                onClick={this.handleClick}>
                {likedSymbol}
            </button>
        )
    }
}