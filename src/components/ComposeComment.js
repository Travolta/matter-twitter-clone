import React from 'react'
import moment from 'moment'

import '../App.css'

export class ComposeComment extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            text: ''
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault()
        const commentPostDate = moment()
        const comment = {
            postDate: commentPostDate,
            noteId: this.props.noteid,
            text: this.state.text
        }
        this.props.updateComments(comment)

        this.setState({ text: '' })
    }

    handleChange(text) {
        this.setState({ text })
    }

    render() {
        return (
            <>
                <form className="compose-comment-form" onSubmit={this.handleSubmit}>
                    <input
                        className="compose-comment-input"
                        value={this.state.text}
                        onChange={event => this.handleChange(event.target.value)}
                        type="text"
                        maxLength="140"
                        required
                    />
                    <br />
                    <input
                        className="compose-comment-submit"
                        type="submit"
                        value="Submit"
                    />
                </form>

            </>
        )
    }
}

