import React from 'react'
import moment from 'moment'

import '../App.css'

export class ComposeNote extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            text: ''
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault()
        const postDate = moment()
        const note = {
            text: this.state.text,
            date: postDate,
            liked: false
        }
        this.props.updateNotes(note)

        this.setState({ text: '' })
    }

    handleChange(text) {
        this.setState({ text })
    }

    render() {
        return (
            <>
                <form onSubmit={this.handleSubmit}>
                    <textarea className="compose-note-input"
                        value={this.state.text}
                        onChange={event => this.handleChange(event.target.value)}
                        type="text"
                        maxLength="140"
                        rows="10"
                        cols="60"
                        required
                    />
                    <br />
                    <input
                        className="compose-note-submit"
                        type="submit"
                        value="Submit"
                    />
                </form>

            </>
        )
    }
}

